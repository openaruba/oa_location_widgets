import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListViewModel.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Models/Region.dart';
import 'package:oa_dart_location_orm/Models/Zone.dart';
import 'package:oa_dart_location_orm/Models/Street.dart';
import 'package:oa_location_widgets/Components/TriTextRowStreet.dart';

void main() {
  test('Street maps correctly to TriTextRowListViewState', () {

    final region = Region.raw("3", "regionname");
    final zone = Zone.raw("2", "zonename", region);
    final street = Street.raw("1", "Name", "abbr", 12345, zone);

    final viewModel = TriTextRowListViewModel(TriTextRowListViewState());

    viewModel.streets([street]);

    assert(viewModel.state.rowStates[0].primaryText == "Name");
    assert(viewModel.state.rowStates[0].subtitle1 == "zonename");
    assert(viewModel.state.rowStates[0].subtitle2 == "abbr");
    assert(viewModel.state.rowStates.length == 1);
  });
}
