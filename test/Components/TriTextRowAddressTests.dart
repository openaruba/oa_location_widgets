import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Models/Location.dart';
import 'package:oa_dart_location_orm/Models/Address.dart';
import 'package:oa_dart_location_orm/Models/Region.dart';
import 'package:oa_dart_location_orm/Models/Zone.dart';
import 'package:oa_dart_location_orm/Models/Street.dart';
import 'package:oa_location_widgets/Components/TriTextRowAddress.dart';

void main() {
  test('Address maps correctly to TriTextRowAddressTests', () {

    final location = Location.raw(1.125, -1.124);
    final region = Region.raw("3", "Oranjestad Oost");
    final zone = Zone.raw("2", "Mon Plesiar", region);
    final street = Street.raw("1", "Byronstraat", "BY", 12345, zone);
    final address = Address.raw("9", "9", street, location);
    final viewModel = TriTextRowListViewModel(TriTextRowListViewState());

    viewModel.addresses([address]);

    assert(viewModel.state.rowStates[0].primaryText == "Byronstraat 9 (BY)");
    assert(viewModel.state.rowStates[0].subtitle1 == "Mon Plesiar");
    assert(viewModel.state.rowStates[0].subtitle2 == "Oranjestad Oost, 12345");
  });
}
