import 'dart:io';

import 'package:dna_flutter_ui_components/Layouts/ContentEmptyErrorLoadingLayout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:oa_dart_location_orm/Models/Pages.dart';
import 'package:oa_dart_location_orm/Models/Region.dart';
import 'package:oa_dart_location_orm/Models/Street.dart';
import 'package:oa_dart_location_orm/Models/Zone.dart';
import 'package:oa_dart_location_orm/ORMCommon/UserException.dart';
import 'package:oa_location_widgets/Widgets/StreetSearchScreen/StreetSearchScreenCoordinator.dart';

void main() {

  final pageMap = Map<String, dynamic>();
  List<Street> streets = List<Street>.empty(growable: true);

  // Helpers
  Future<Pages<Street>> _fetchStreets([int page = 1, String? search, String? id, String? regionId]) async {
    try {
      return Pages<Street>(pageMap, streets);
    } catch(error) {
      throw UserException(error as Exception);
    }
  }

  Future<Pages<Street>> _fetchStreetsError([int page = 1, String? search, String? id, String? regionId]) async {
    try {
      final Error error = Error();
      throw SocketException("Some error");
      return Pages<Street>(pageMap, streets);
    } catch(error) {
      throw UserException(error as Exception);
    }
  }

  // Tests
  test('Default state is correct', () {
    final environment = StreetSearchScreenEnvironment(_fetchStreets);
    final coordinator = StreetSearchScreenCoordinator(environment);
    assert(coordinator.searchBarViewModel.state.formKey == "search");
    assert(coordinator.searchBarViewModel.state.hintText == "Search here");
    assert(coordinator.searchBarViewModel.state.inputType == TextInputType.text);
  });

  test('Check if contentEmptyErrorLoadingLayoutState starts with loading', () {

    final environment = StreetSearchScreenEnvironment(_fetchStreets);
    final coordinator = StreetSearchScreenCoordinator(environment);

    assert(coordinator.contentEmptyErrorLoadingLayoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.loading);
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.state.title == "OpenAruba");
  });

  test('ContentEmptyErrorLoadingLayoutState shows empty when we return no streets', () async {

    final environment = StreetSearchScreenEnvironment(_fetchStreets);
    final coordinator = StreetSearchScreenCoordinator(environment);

    await Future.delayed(const Duration(milliseconds: 100), (){});

    assert(coordinator.contentEmptyErrorLoadingLayoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.message);
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.primaryMessage == "No match");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.secondaryMessage == "No match for filter, please clear text above for more results.");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.action == "");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.hasAction == false);
  });

  test('ContentEmptyErrorLoadingLayoutState shows content when we have streets', () async {

    final environment = StreetSearchScreenEnvironment(_fetchStreets);
    final coordinator = StreetSearchScreenCoordinator(environment);

    streets.add(Street.raw("1", "Madurostraat", "mdr", 12345, Zone.raw("4", "zone", Region.raw("3", "region"))));
    streets.add(Street.raw("2", "Byronstraat", "byr", 12345, Zone.raw("4", "zone", Region.raw("3", "region"))));

    await Future.delayed(const Duration(milliseconds: 100), (){});

    assert(coordinator.contentEmptyErrorLoadingLayoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.content);
    assert(coordinator.streetListViewModel.rowsCount == 2);
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.primaryMessage == "");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.secondaryMessage == "");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.action == "");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.hasAction == false);
    streets.clear();
  });

  test('ContentEmptyErrorLoadingLayoutState filters streets correctly', () async {

    final environment = StreetSearchScreenEnvironment(_fetchStreets);
    final coordinator = StreetSearchScreenCoordinator(environment);

    streets.add(Street.raw("1", "Madurostraat", "mdr", 12345, Zone.raw("4", "zone", Region.raw("3", "region"))));
    streets.add(Street.raw("2", "Byronstraat", "byr", 12345, Zone.raw("4", "zone", Region.raw("3", "region"))));

    await Future.delayed(const Duration(milliseconds: 100), (){});

    coordinator.searchBarViewModel.value.add("By");

    await Future.delayed(const Duration(milliseconds: 1000), (){});

    assert(coordinator.contentEmptyErrorLoadingLayoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.content);
    assert(coordinator.streetListViewModel.rowsCount == 1);
    streets.clear();
  });

  test('ContentEmptyErrorLoadingLayoutState filtering all shows no match', () async {

    final environment = StreetSearchScreenEnvironment(_fetchStreets);
    final coordinator = StreetSearchScreenCoordinator(environment);

    streets.add(Street.raw("1", "Madurostraat", "mdr", 12345, Zone.raw("4", "zone", Region.raw("3", "region"))));
    streets.add(Street.raw("2", "Byronstraat", "byr", 12345, Zone.raw("4", "zone", Region.raw("3", "region"))));

    await Future.delayed(const Duration(milliseconds: 100), (){});

    coordinator.searchBarViewModel.value.add("xy");

    await Future.delayed(const Duration(milliseconds: 100), (){});

    assert(coordinator.contentEmptyErrorLoadingLayoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.message);
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.primaryMessage == "No match");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.secondaryMessage == "No match for filter, please clear text above for more results.");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.action == "");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.hasAction == false);
    streets.clear();
  });

  test('ContentEmptyErrorLoadingLayoutState fetch throws error', () async {

    final environment = StreetSearchScreenEnvironment(_fetchStreetsError);
    final coordinator = StreetSearchScreenCoordinator(environment);

    await Future.delayed(const Duration(milliseconds: 100), (){});

    assert(coordinator.contentEmptyErrorLoadingLayoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.message);
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.primaryMessage == "Error");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.secondaryMessage == "There has been a connection error. Please check your connection and try again.");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.action == "Retry");
    assert(coordinator.contentEmptyErrorLoadingLayoutModel.errorViewModel.state.hasAction == true);
    streets.clear();
  });
}


