import 'package:dna_flutter_ui_components/Components/TriTextRow/TriTextRowView.dart';
import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:oa_dart_location_orm/Models/Address.dart';

extension TriTextRowAddress on TriTextRowListViewModel {
  void addresses(List<Address> addresses) {
    final rows = addresses.map((address) {
      final addressLine = (address.street.name ?? "") + " " + (address.number ?? "")  + " (" + (address.street.abbreviation ?? "") + ")";
      final zone = address.street.zone.name ?? "";
      final region = (address.street.zone.region.name ?? "") + ", " + (address.street.gacCode?.toString() ?? "");
      return TriTextRowViewState(addressLine, zone, region);
    }).toList();
    this.rows(rows);
  }
}