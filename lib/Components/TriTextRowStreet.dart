import 'package:dna_flutter_ui_components/Components/TriTextRow/TriTextRowView.dart';
import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:oa_dart_location_orm/Models/Street.dart';

extension TriTextRowStreet on TriTextRowListViewModel {
  void streets(List<Street> streets) {
    final rows = streets.map((street) => TriTextRowViewState(
        street.name ?? "",
        street.zone.name ?? "",
        street.abbreviation ?? ""
    )).toList();
    this.rows(rows);
  }
}