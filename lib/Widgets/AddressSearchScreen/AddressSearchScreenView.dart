import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:dna_flutter_ui_components/Layouts/ContentEmptyErrorLoadingLayout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'AddressSearchCoordinator.dart';

class AddressSearchScreenView extends StatelessWidget{

  final AddressSearchScreenCoordinator coordinator;

  const AddressSearchScreenView(this.coordinator);

  @override
  Widget build(BuildContext context) {
    return ContentEmptyErrorLoadingLayout(
        this.coordinator.contentEmptyErrorLoadingLayoutModel,
        null,
        TriTextRowListView(coordinator.addressListViewModel));
  }
}