
import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:dna_flutter_ui_components/Layouts/ContentEmptyErrorLayoutModel.dart';
import 'package:dna_flutter_ui_components/Layouts/ContentEmptyErrorLoadingLayout.dart';
import 'package:oa_dart_location_orm/Models/Address.dart';
import 'package:oa_dart_location_orm/Models/Pages.dart';
import 'package:oa_location_widgets/Components/TriTextRowAddress.dart';
import 'package:rxdart/rxdart.dart';

class AddressSearchScreenEnvironment {
  final Function fetchAddresses;
  AddressSearchScreenEnvironment(var fetchAddresses)
      : this.fetchAddresses = fetchAddresses;
}

class AddressSearchScreenCoordinator {

  late final Stream<Address> didSelectStreet;

  static AddressSearchScreenCoordinator create(String search) {
    return AddressSearchScreenCoordinator(AddressSearchScreenEnvironment(() {
        return Address.all(1, search);
    }));
  }

  final ContentEmptyErrorLoadingLayoutModel contentEmptyErrorLoadingLayoutModel;
  final TriTextRowListViewModel addressListViewModel;

  // MARK: Private

  final AddressSearchScreenEnvironment _environment;

  AddressSearchScreenCoordinator(AddressSearchScreenEnvironment environment):
        _environment = environment,
        contentEmptyErrorLoadingLayoutModel = ContentEmptyErrorLoadingLayoutModel(ContentEmptyErrorLoadingLayoutState("Addresses")),
        addressListViewModel = TriTextRowListViewModel(TriTextRowListViewState()){

    // Request and retries
    final userInitiatedFetchStreet = Future<void>.microtask(() => {  }).asStream()
        .mergeWith([contentEmptyErrorLoadingLayoutModel.errorViewModel.retryTapped.stream])
        .shareReplay();

    userInitiatedFetchStreet.listen((event) {
      contentEmptyErrorLoadingLayoutModel.showLoading();
    });

    final fetchAddresses = userInitiatedFetchStreet
        .flatMap((value) => _environment.fetchAddresses().asStream())
        .materialize()
        .shareReplay();

    // On success
    final fetchAddressesSuccess = fetchAddresses
        .where((event) => event.isOnData)
        .dematerialize()
        .shareReplay();

    fetchAddressesSuccess.listen((pages) {
      addressListViewModel.addresses(pages.objects);
      if(pages.objects.isEmpty) {
        contentEmptyErrorLoadingLayoutModel.showMessage("No match", "No match for filter, please clear text above for more results.", false, "");
      }
      else {
        contentEmptyErrorLoadingLayoutModel.showContent();
      }
    });

    // On error
    fetchAddresses
        .where((event) => event.isOnError)
        .dematerialize()
        .onErrorReturnWith((error, stackTrace) => error)
        .listen((event) {
          contentEmptyErrorLoadingLayoutModel.showMessage("TODO:", "TODO2:", true, "Retry");
        });

    didSelectStreet = addressListViewModel.indexTapped
        .withLatestFrom(fetchAddressesSuccess, (index, pages) {
            return (pages as Pages<Address>).objects[index];
        });
  }
}