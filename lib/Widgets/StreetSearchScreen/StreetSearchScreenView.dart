import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:dna_flutter_ui_components/Components/SearchBar/SearchBarView.dart';
import 'package:dna_flutter_ui_components/Layouts/ContentEmptyErrorLoadingLayout.dart';

import 'package:flutter/material.dart';
import 'StreetSearchScreenCoordinator.dart';

class StreetSearchScreenView extends StatelessWidget {

  final StreetSearchScreenCoordinator coordinator;

  const StreetSearchScreenView(this.coordinator);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: TextTheme(
            headline6: TextStyle(color: Colors.black)
        ),
        primarySwatch: Colors.blue,
      ),
      home: ContentEmptyErrorLoadingLayout(
          this.coordinator.contentEmptyErrorLoadingLayoutModel,
          SearchBarView(coordinator.searchBarViewModel),
          TriTextRowListView(coordinator.streetListViewModel))
    );
  }
}
