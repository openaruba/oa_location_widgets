import 'dart:async';
import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:dna_flutter_ui_components/Components/SearchBar/SearchBarView.dart';
import 'package:dna_flutter_ui_components/Layouts/ContentEmptyErrorLoadingLayout.dart';
import 'package:oa_dart_location_orm/Models/Pages.dart';
import 'package:oa_dart_location_orm/Models/Street.dart';
import 'package:oa_location_widgets/Components/TriTextRowStreet.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class StreetSearchScreenEnvironment {
  final Function() fetchStreets;
  StreetSearchScreenEnvironment(var fetchStreets)
      : this.fetchStreets = fetchStreets;
}

class StreetSearchScreenCoordinator extends ChangeNotifier {

  static StreetSearchScreenCoordinator create() {
    return StreetSearchScreenCoordinator(StreetSearchScreenEnvironment(Street.all));
  }

  // MARK: Outputs

  StreamController<Street> didTapStreet = StreamController<Street>();
  StreamController<String> didSearch = StreamController<String>();

  // MARK: ViewModels

  final TriTextRowListViewModel streetListViewModel;
  final SearchBarViewModel searchBarViewModel;
  final ContentEmptyErrorLoadingLayoutModel contentEmptyErrorLoadingLayoutModel;

  // MARK: Environment

  final StreetSearchScreenEnvironment _environment;

  StreetSearchScreenCoordinator(StreetSearchScreenEnvironment environment) :
        this._environment = environment,
        this.searchBarViewModel = SearchBarViewModel(SearchBarViewState("search", "Search here", TextInputType.text)),
        this.streetListViewModel = TriTextRowListViewModel(TriTextRowListViewState()),
        this.contentEmptyErrorLoadingLayoutModel = ContentEmptyErrorLoadingLayoutModel(ContentEmptyErrorLoadingLayoutState("OpenAruba")) {

    // Request and retries
    final userInitiatedFetchStreet = Future<void>.microtask(() => {  }).asStream()
        .mergeWith([contentEmptyErrorLoadingLayoutModel.errorViewModel.retryTapped.stream])
        .shareReplay();

    userInitiatedFetchStreet.listen((event) { _loading(); });

    searchBarViewModel.submit.stream
        .pipe(this.didSearch);

    final fetchStreets = userInitiatedFetchStreet
        .flatMap((value) => _environment.fetchStreets().asStream())
        .materialize()
        .shareReplay();

    // On success
    final didFetchStreets = fetchStreets
        .where((event) => event.isOnData)
        .dematerialize()
        .shareReplay();

    final filteredStreets = CombineLatestStream.combine2(
        didFetchStreets,
        searchBarViewModel.value.stream.startWith(""), (streets, filter) => {
          (streets as Pages<Street>).objects.where((element) => element.filters(filter as String)).toList()
        })
        .map((filtered) => filtered.last)
        .shareReplay();

    filteredStreets.listen((filtered) {
          streetListViewModel.streets(filtered);
          if(filtered.isEmpty) {
            contentEmptyErrorLoadingLayoutModel.showMessage("No match", "No match for filter, please clear text above for more results.", false, "");
          }
          else {
            contentEmptyErrorLoadingLayoutModel.showContent();
          }
        notifyListeners();
    });

    // On fetch error
    fetchStreets
        .where((event) => event.isOnError)
        .dematerialize()
        .onErrorReturnWith((error, stackTrace) => error)
        .listen((event) {
          _didError(event.userMessage);
         });

    // Index to street
    streetListViewModel.indexTapped
        .withLatestFrom(filteredStreets, (index, filteredStreets) => (filteredStreets as List<Street>)[index])
        .pipe(didTapStreet);
  }

  // MARK: Private helper methods

  void _loading() {
    contentEmptyErrorLoadingLayoutModel.showLoading();
    notifyListeners();
  }

  void _didError(String errorMessage) {
    contentEmptyErrorLoadingLayoutModel.showMessage("Error", errorMessage, true, "Retry");
    notifyListeners();
  }
}