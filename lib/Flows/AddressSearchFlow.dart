// import 'package:dna_reactive_navigator/ReactiveNavigator.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:rxdart/rxdart.dart';
//
// abstract class LoginFlowProviderType {
//   // // Page one is the root widget, item two is the didClick stream
//   // Tuple3<StatelessWidget, Stream<void>, Stream<void>> rootWidget();
//   //
//   // // Page two is the second screen widget, item two is the didClick stream
//   // Tuple2<MaterialPageRoute, Stream<void>> secondScreen(int);
//   //
//   // // Page two is the second screen widget, item two is the didClick stream
//   // Tuple2<MaterialPageRoute, Stream<void>> registerScreen();
// }
//
// class AddressSearchFlow {
//
//   final ReactiveNavigator _nav;
//   final BuildContext _context;
//
//   StatelessWidget get rootWidget { return this._root.item1;}
//
//   // Privates
//   //final Tuple3<StatelessWidget, Stream<void>, Stream<void>> _root;
//
//   AddressSearchFlow(BuildContext context, LoginFlowProviderType provider, [ReactiveNavigator nav]):
//         _nav = nav ?? ReactiveNavigator(context),
//         this._context = context,
//         this._root = provider.rootWidget() {
//
//     // final page2 = _root.item2.map((event) => provider.secondScreen(87))
//     //     .shareReplay();
//     //
//     // // Register page
//     // final registerPage = this._root.item3.map((event) => provider.registerScreen())
//     //     .shareReplay();
//     //
//     // final registerPageGoBack = registerPage.flatMap((event) => event.item2);
//     //
//     // // Push screen two
//     // MergeStream([
//     //   registerPage.map((event) => event.item1),
//     //   page2.map((event) => event.item1),
//     // ]).pipe(_nav.push);
//     //
//     // // Create new stream of the go back click to be piped in pop in order to go back
//     // MergeStream([registerPageGoBack, page2.flatMap((value) => value.item2)])
//     //     .pipe(_nav.pop);
//   }
// }